---
# Metadata
title: Rapport de stage de M2
author: Lucas Verney
date: \today
lang: fr-FR
# LaTeX headers
numbersections: True
header-includes:
    - \usepackage{subcaption}
    - \expandafter\def\csname ver@subfig.sty\endcsname{}  # Black magic, https://tex.stackexchange.com/questions/213273/undefined-control-sequence-at-begindocument
    - \usepackage{dsfont}
    - \usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
    - \renewcommand{\arraystretch}{1.5}
# Pandoc-crossref options
cref: True
chapters: True
# Pandoc-citeproc options
bibliography: notes.bib
link-citations: True
reference-section-title: Bibliographie
nocite:
---

\pagebreak

\section*{Remerciements}

Je tiens à remercier M. Mazyar Mirrahimi, directeur de recherche au sein de
l'équipe QUANTIC à l'INRIA, et M. Zaki Leghtas, chercheur aux Mines-Paristech,
pour m’avoir proposé ce sujet de stage, pour leur disponibilité et leurs
conseils précieux tout au long de ce stage.

Je remercie également l’ensemble de l’équipe QUANTIC de l'INRIA, de l'ENS et
des Mines-Paristech pour son accueil.

\pagebreak


# Introduction

[@DevoretSchoelpkof2013] distingue sept grandes étapes dans le traitement de
l'information quantique, depuis la réalisation d'opérations simples sur des
qubits physiques individuels, jusqu'à la réalisation d'un calcul quantique
complet et résistant aux erreurs. Les systèmes à base de qubits
supraconducteurs sont un des seuls systèmes de physique du solide ayant
atteint la troisième étape (« être en mesure de faire des mesures quantique
non-destructives (QND) pour la correction d'erreurs et le contrôle ») et en
passe d'atteindre la quatrième étape (« manipuler des qubits logiques, avec de
meilleurs temps de vie que les qubits physiques »), tout comme les systèmes
d'optique atomique à base d'ions piégés ou d'atomes de Rydberg.

En information quantique, l'information est encodée sur un qubit, un système à
deux niveaux analogue quantique du bit d'information classique. Classiquement,
la correction d'erreurs utilise la redondance et des votes de majorité. Des
erreurs peuvent apparaître également sur des systèmes quantiques, qu'il faut
alors corriger. En effet, la décohérence d'un bit quantique causée par son
interaction avec un environnement bruité peut être modélisée par des erreurs
du type inversion de bit (similaires aux inversions classiques) et inversion
de phase. Le théorème de non-clonage empêche de copier l'information et
d'exploiter des redondances aussi simplement, et il faut alors trouver
d'autres schémas de correction d'erreurs quantique afin de stabiliser les
états des qubits. En théorie, un système implémentant de la correction
d'erreurs quantique peut passer un seuil et préserver de l'information pendant
des durées plus longues que la durée de vie de chacun de ses constituants.

Pour implémenter la correction d'erreurs quantique, on encode de façon
redondante l'état du qubit dans un espace de plus grande dimension, en
utilisant des états quantiques avec des symétries astucieuses. En réalisant
une mesure projective utilisant ces symétries, on peut détecter les erreurs
éventuelles et les corriger en appliquant des opérations simples sur le qubit.

[@PRL111-120501; @NJP2014; @Andrei] ont proposé un système utilisant un oscillateur
harmonique comme qubit logique. Ainsi, les états du qubit sont encodés sur des
superpositions d'états cohérents, dans l'espace de Hilbert de dimension
infinie de l'oscillateur harmonique. De plus, les états cohérents sont des
états propres de l'opérateur annihilation. On peut alors contraindre la
dynamique à s'effectuer dans un sous-espace bien contrôlé et stabilisé, par
ingénierie de réservoir, et bénéficier de meilleurs temps de vie pour le
qubit, en étant limité principalement par le facteur de qualité de la cavité.

[@PRL111-120501] propose d'encoder l'information dans des états chat de
Schrödinger, superposition d'états cohérents, pour encoder l'information
quantique:
\begin{equation}
    \left|C^{\pm}_{\alpha}\right> = \mathcal{N} \Big(
        \left|\alpha\right> \pm
        \left|-\alpha\right>\Big)
    \text{ et }
    \left|C^{\pm}_{i \alpha}\right> = \mathcal{N} \Big(
        \left|i \alpha\right> \pm
        \left|-i \alpha\right>\Big)
\end{equation}
où $\mathcal{N}$ désigne un facteur de normalisation qu'on ne cherche pas à
expliciter. Les états cohérents $\left|\alpha\right>$, $\left|-\alpha\right>$,
$\left|i\alpha\right>$ et $\left|-i\alpha\right>$ ne sont pas orthogonaux,
mais pour des valeurs de $\alpha$ suffisamment grandes (en pratique, pour
$\alpha \gtrsim 2$), ils sont quasiment orthogonaux.

L'encodage de l'information sur ces états la protège contre des pertes de
photons. En effet, $\text{Vect}\Big(\left|\alpha\right>, \left|-\alpha\right>,
\left|i\alpha\right>, \left|-i\alpha\right>\Big)$ est un sous-espace stable
par application de l'opérateur annihilation. On peut de plus suivre les pertes
de photons en mesurant la valeur de l'opérateur parité $\Pi = \exp \left( i
\pi a^{\dagger} a \right)$. En effet, notons déjà que $a
\left|C_{\alpha}^{\pm}\right> \propto \left|C_{\alpha}^{\mp}\right>$ et $a
\left|C_{i \alpha}^{\pm}\right> \propto i \left|C_{i\alpha}^{\mp}\right>$. En
introduisant
\begin{equation}\left\{\begin{array}{l}
    \left|\Psi_\alpha^0\right> = c_0 \left|C_{\alpha}^+ \right> + c_1 \left|C_{i \alpha}^+\right>, \\
    \left|\Psi_\alpha^1\right> = c_0 \left|C_{\alpha}^- \right> + i c_1 \left|C_{i \alpha}^-\right>, \\
    \left|\Psi_\alpha^2\right> = c_0 \left|C_{\alpha}^+ \right> - c_1 \left|C_{i \alpha}^+\right>, \\
    \left|\Psi_\alpha^3\right> = c_0 \left|C_{\alpha}^- \right> - i c_1 \left|C_{i \alpha}^-\right>,
\end{array}\right.\end{equation}
afin de représenter l'état $c_0 \left|0\right> + c_1 \left|1\right>$ du qubit,
et en notant que $a \left|\Psi_{\alpha}^k\right> \propto
\left|\Psi_{\alpha}^{k + 1 \text{ mod } 4}\right>$, on observe qu'une perte
d'un photon unique correspond à un changement de base d'encodage de l'état du
qubit. Comme de plus $\left<\Psi_{\alpha}^k \right| \Pi \left| \Psi_{\alpha}^k \right> = (-1)^k$, une perte d'un photon se traduit par
un saut de la valeur moyenne de l'observable de parité.

Cependant, bien que de tels états permettent de garder une trace des pertes de
photons, ils sont toujours sensibles à la relaxation d'énergie déterministe,
qui fait évoluer les états cohérents $\left|\alpha\right>$ comme $\left|\alpha
e^{-\kappa t / 2}\right>$ où $\kappa$ est le taux de dissipation, lié au
facteur de qualité de la cavité. Pour stabiliser dans le temps la variété
engendrée par ces états, et dépasser cette dissipation d'énergie, il faut
réaliser une interaction non-linéaire qui permet de réinjecter de l'énergie
dans le système, comme réalisé dans [@ZakiScience2015]. En effet, [@NJP2014;
@ZakiScience2015] proposent de stabiliser cette variété d'états en utilisant
une méthode d'ingéniérie de réservoir. Ils proposent d'ajuster le hamiltonien
de couplage entre le mode de stockage de l'information, dans la cavité, et son
environnement pour stabiliser l'énergie de l'état chat de Schrödinger qui
encode l'information. Pour ce faire, il se base sur l'idée qu'envoyer un champ
à résonance sur un oscillateur harmonique dissipatif stabilise un état
cohérent du mode du champ de la cavité. De manière similaire, si on couple un
oscillateur harmonique quantique à un bain dont on restreint les échanges à se
faire par paires de photons, on peut restreindre les états de l'oscillateur
harmonique au sous-espace engendré par les états $\left|C^+_{\alpha}\right>$
et $\left|C^-_{\alpha}\right>$ précédents. Ceci revient à réaliser un
hamiltonien de la forme
\begin{equation}
    H = \hbar\, g_{\text{2ph}} \left[b \left(c^{\dagger}\right)^2 + \text{h.c.}\right]
\end{equation}
où $b$ est un mode de lecture (réservoir) et $c$ est le mode de stockage de la
cavité. Le mode de lecture est couplé à une ligne de transmission et a un
facteur de qualité bas, lui permettant d'évacuer l'entropie du système. Le
mode de stockage a un haut facteur de qualité et stocke l'information.

En forçant des échanges à deux photons entre le mode de stockage de la cavité
et un mode de lecture (réservoir), il est donc possible de stabiliser cette
variété d'états sur de plus longues durées, tout en étant résistant
aux erreurs de déphasage. En revanche, il n'est pas possible de résister aux
erreurs de type inversion de bit liées à des pertes de photons uniques.
L'expérience a été réalisée dans [@ZakiScience2015] et vérifie
expérimentalement le fonctionnement de ce système, en atteignant des durées de
vie du qubit logique du même ordre de grandeur que les temps caractéristiques
de la dissipation de la cavité.

[@NJP2014] propose donc une extension du schéma en utilisant des échanges à
quatre photons, qui permet de résister aux inversions de bit, à travers un
hamiltonien de la forme
\begin{equation}
    H = \hbar\, g_{\text{4ph}} \left[ b \left(c^{\dagger}\right)^4 + \text{h.c.}\right],
\end{equation}
où $b$ est un mode de lecture (réservoir) et $c$ est le mode de stockage de la
cavité, en cherchant à avoir $g_{\text{4ph}}$ le plus grand possible
(typiquement de l'ordre de quelques MHz).

Pour ce faire, ils proposent d'utiliser un circuit faisant du mélange à six
ondes, avec un hamiltonien de la forme
\begin{equation}
    H = \hbar\, g_{\text{4ph}} \left[ a b \left(c^{\dagger}\right)^4 + \text{h.c.}\right],
\end{equation}
où le mode $a$ supplémentaire est couplé à une pompe et où on impose de plus
une condition d'accord de fréquence $\omega_a + \omega_b - 4\omega_c = 0$.

Après élimination adiabatique du mode $b$ [@NJP2014; @Carmichael], on peut
écrire une équation maîtresse sur le mode $c$ uniquement, de la forme
\begin{equation}
    \dot{\rho} = \left[
        \varepsilon_{\text{4ph}} \left(c^{\dagger}\right)^4 -
        \varepsilon_{\text{4ph}}^* c^4
    \right] + \kappa_{\text{4ph}} D[c^4] \rho
\end{equation}
avec $\varepsilon_{\text{4ph}} = \frac{2 \varepsilon_{\text{pompe}}
g_{\text{4ph}}}{\kappa_b}$ et $\kappa_{\text{4ph}} = \frac{4
g_{\text{4ph}}^2}{\kappa_b}$ où $\varepsilon_{\text{pompe}}$ est
l'amplitude du champ de la pompe et $\kappa_b$ est le taux de relaxation à un
photon du mode $b$.


On souhaite avoir $\kappa_{\text{4ph}} \gg \kappa_{\text{1ph}}$, le taux de
dissipation à un photon. En notant que $\kappa_{\text{4ph}} < g_{\text{4ph}}$
et que dans les expériences on a $\kappa_{\text{1ph}} \approx 10 \text{kHz}$,
nous cherchons à atteindre des couplages non-linéaires $g_{\text{4ph}}$ de
l'ordre de $1 \text{MHz}$.

Le but de ce stage était de reprendre la proposition de circuits de [@NJP2014],
basée sur un modulateur en anneau Josephson modifié, permettant d'avoir un
hamiltonien de la forme $b \left(c^{\dagger}\right)^4 + \text{h.c.}$ afin
de stabiliser les états de chats, d'en étudier les propriétés et de réfléchir
à des alternatives plus simples au vu des contraintes de nanofabrication et
de limitation des effets parasites.


# Notations

On note $\varphi_k = \Phi_k / \varphi_0$ le flux réduit, où
$\varphi_0 = \hbar / 2e$ est le quantum de flux réduit.

On considère les éléments suivants dans les circuits :

- des jonctions Josephson (![](img/junction.svg){width=40px}), dont la relation phase-intensité est
\begin{equation}
    i_k = I_0 \sin \varphi_k
\end{equation}
avec $I_0$ l'intensité critique, et dont le hamiltonien est
\begin{equation}
    H = -E_J \cos \varphi_k
\end{equation}
où $E_J = I_0 \varphi_0$.

- des inductances (![](img/inductor.svg){width=40px}), dont la relation phase-intensité est
\begin{equation}
    \Phi_k = L i_k,
\end{equation}
et dont le hamiltonien est
\begin{equation}
    H = E_L \frac{\varphi_k^2}{2}
\end{equation}
où $E_L = \varphi_0^2 / L$.


# Modulateur en anneau Josephson

Dans un premier temps, rappelons quelques résultats antérieurs sur le
modulateur en anneau Josephson (JRM) (et sa version avec inductances de
_shunt_), tels que présentés dans [@BergealNature; @1202.1315; @ThèseManu,
@ThèseFlavius], avant de décrire et étudier plus en détails le circuit
modifié, proposé dans [@NJP2014]. Cette partie reprend et commente les calculs
présentés dans [@ThèseManu].

## Modulateur en anneau Josephson de base

![Schéma électrique du modulateur en anneau Josephson de base, tel qu'introduit
dans @BergealNature. Schéma de [@ThèseFlavius].](img/standard_JRM.svg){#fig:JRM}

Le hamiltonien du système ([@fig:JRM]) s'écrit
\begin{equation}
    H = -E_J \left(
        \cos \varphi_a + \cos \varphi_b +
        \cos \varphi_c + \cos \varphi_d \right)
\end{equation}
avec
$$
    \left\{\begin{array}{c}
        \varphi_a = \varphi_1 - \varphi_4 + \frac{\varphi_{\text{ext}}}{4} + n \frac{\pi}{2} \\
        \varphi_b = \varphi_4 - \varphi_2 + \frac{\varphi_{\text{ext}}}{4} + n \frac{\pi}{2} \\
        \varphi_c = \varphi_2 - \varphi_3 + \frac{\varphi_{\text{ext}}}{4} + n \frac{\pi}{2} \\
        \varphi_d = \varphi_3 - \varphi_1 + \frac{\varphi_{\text{ext}}}{4} + n \frac{\pi}{2} \\
    \end{array}\right.
$${#eq:JRM_mailles}
où $\varphi_{\text{ext}}$ est le flux extérieur traversant l'anneau du modulateur.

La loi des mailles nous donne par ailleurs
$$
    \varphi_a + \varphi_b + \varphi_c + \varphi_d = \varphi_{\text{ext}} [2\pi]
$${#eq:JRM_maille}

En introduisant les modes « normaux », obtenus en cherchant un changement de
base des $\varphi_{1, 2, 3, 4}$ qui permette d'écrire la partie quadratique du
hamiltonien comme $\boldsymbol{{}^t\varphi} \mathcal{L} \boldsymbol{\varphi}$
avec $\mathcal{L}$ diagonale et $\boldsymbol{{}^t \varphi} = \begin{pmatrix}
\varphi_X & \varphi_Y & \varphi_Z & \varphi_M \end{pmatrix}$, donnés par
$$
    \begin{pmatrix} \varphi_X \\ \varphi_Y \\ \varphi_Z \\ \varphi_M \end{pmatrix} =
    \begin{pmatrix}
        1/2 & -1/2 & -1/2 & 1/2 \\
        1/2 & 1/2 & -1/2 & -1/2 \\
        1/4 & -1/4 & 1/4 & -1/4 \\
        1 & 1 & 1 & 1
    \end{pmatrix}
    \begin{pmatrix} \varphi_a \\ \varphi_b \\ \varphi_c \\ \varphi_d \end{pmatrix}.
$${#eq:normal_modes}

le hamiltonien se simplifie en
$$\boxed{
\begin{split}
    H = -4E_J \left[
        \cos \frac{\varphi_X}{2} \cos \frac{\varphi_Y}{2} \cos \varphi_Z \cos \left( \frac{\varphi_{\text{ext}} + 2 n \pi}{4} \right) + \right. \\
        \left. \sin \frac{\varphi_X}{2} \sin \frac{\varphi_Y}{2} \sin \varphi_Z \sin \left( \frac{\varphi_{\text{ext}} + 2 n \pi}{4} \right)
    \right].
\end{split}
}$${#eq:JRM_hamiltonian}

_Remarque_: $\varphi_M = \varphi_{\text{ext}} + 2 n \pi$ et sa valeur propre de
$\mathcal{L}$ associée est nulle.

En linéarisant le hamiltonien [@eq:JRM_hamiltonian] autour du point d'équilibre
au repos ($\varphi_X = \varphi_Y = \varphi_Z = 0$), on obtient
\begin{equation}\begin{split}
    H = & -4 E_J \cos \left( \frac{\varphi_{\text{ext}} + 2 n \pi}{4} \right) \\
        & + \frac{E_J}{2} \cos \left( \frac{\varphi_{\text{ext}} + 2 n \pi}{4} \right) \left(\varphi_X^2 + \varphi_Y^2 + 4 \varphi_Z^2 \right) \\
        & - E_J \sin \left( \frac{\varphi_{\text{ext}} + 2 n \pi}{4} \right) \varphi_X \varphi_Y \varphi_Z
\end{split}\end{equation}

On voit apparaître un terme de mélange à trois ondes en $\varphi_X \varphi_Y \varphi_Z$,
dont l'amplitude dépend du flux extérieur traversant la boucle du _JRM_. C'est
ce terme qui est intéressant dans le cas d'un fonctionnement en amplification
ou en conversion. On utilise alors deux modes comme signal et _idler_ et le
troisième mode comme pompe.

L'amplitude du terme de mélange à trois ondes est maximale pour un flux
extérieur $\varphi_{\text{ext}} = 2\pi$, donnant alors le meilleur gain
possible. Cependant, cette configuration est instable et ne peut être atteinte
(voir [@fig:JRM_instable]).

![Illustration de l'instabilité du _JRM_ (figure de @ThèseManu). La loi des
mailles donne une relation entre les flux modulo $2\pi$ ([@eq:JRM_maille]).
Chaque configuration (donnée par les flux $\varphi_{a, b, c, d}$) est donc
$8\pi$-périodique. Mais, le niveau d'énergie le plus bas du hamiltonien
[@eq:JRM_hamiltonian] $E_{\text{rest}}$ dépend de la valeur du multiple de
$2\pi$, $n$. En effet, si le système démarre sur la branche $n=4p$, avec un
flux magnétique réduit extérieur $\varphi_{\text{ext}} = 0$, et que la valeur
du flux magnétique extérieur augmente jusqu'à $\pi$, la branche $n=4p$ devient
métastable et c'est la branche $n=4p+1$ qui est la plus stable. Le système
subit alors un glissement vers cette configurationn, correspondant à un
échange de quantum de flux, ce qui empêche d'atteindre le point de
fonctionnement optimal, $\varphi_{\text{ext}} = 2\pi$. Le meilleur compromis
est alors autour de $\varphi_{\text{ext}} =
\pi$.](img/JRM_instable.png){#fig:JRM_instable}


\pagebreak

## Modulateur en anneau Josephson avec inductances de _shunt_ {#sec:JRM-shunt}

Pour résoudre le problème précédent de saut de phase, une solution est de
rajouter des inductances de _shunt_ à la boucle de jonctions Josephson
([@fig:JRM_shunted]).

![Schéma électrique du modulateur en anneau Josephson avec inductances de _shunt_,
tel que présenté dans @ThèseFlavius. On note $E_L$ l'énergie associée aux
inductances de _shunt_ et $E_J$ celle associée aux jonctions
Josephson.](img/shunted_JRM.svg){#fig:JRM_shunted}

Je vais reprendre et détailler le calcul du hamiltonien et la quantification
de ce système, présentés dans [@ThèseManu], en suivant la méthode présentée
dans [@DevoretCdF].

Il faut tout d'abord choisir une électrode de référence (masse). Au vu des
symétries de ce circuit, nous choisissons le nœud central, au milieu des
inductances de _shunt_ comme électrode de référence. Il faut ensuite trouver
un arbre couvrant le circuit, c'est à dire une liste de branches qui passe par
chaque nœud, sans boucle. Dans notre cas, l'arbre couvrant peut être choisi
comme étant les quatre branches des quatre inductances de _shunt_, qui
contient bien les nœuds $1$, $2$, $3$ et $4$ du circuit, ainsi que l'électrode
de référence.

On introduit ensuite les flux sur les branches de l'arbre : $\varphi_{L_1}$,
$\varphi_{L_2}$, $\varphi_{L_3}$ et $\varphi_{L_4}$, qui sont égaux au flux
aux points $1$, $2$, $3$ et $4$ d'après le choix de notre électrode de
référence.

Le hamiltonien du système s'écrit
\begin{equation}\begin{split}
    H = & -E_J \left(
            \cos \varphi_a + \cos \varphi_b +
            \cos \varphi_c + \cos \varphi_d \right) \\
        & + \frac{E_L}{2} \left(
            \varphi_{L_1}^2 + \varphi_{L_2}^2 +
            \varphi_{L_3}^2 + \varphi_{L_4}^2 \right) \\
\end{split}\end{equation}

Dans ce cas, les équations du circuit données par les lois des mailles dans
chaque petite maille de la boucle sont
$$
    \left\{\begin{array}{c}
        \varphi_a - \varphi_{L_1} + \varphi_{L_4} = \frac{\varphi_{\text{ext}}}{4} + 2\pi n_a \\
        \varphi_b - \varphi_{L_4} + \varphi_{L_2} = \frac{\varphi_{\text{ext}}}{4} + 2\pi n_b \\
        \varphi_c - \varphi_{L_2} + \varphi_{L_3} = \frac{\varphi_{\text{ext}}}{4} + 2\pi n_c \\
        \varphi_d - \varphi_{L_3} + \varphi_{L_1} = \frac{\varphi_{\text{ext}}}{4} + 2\pi n_d \\
    \end{array}\right.
$${#eq:JRM_shunt_mailles}
où $\varphi_{\text{ext}}$ est le flux extérieur traversant la totalité de l'anneau.

La conservation de la charge au point central donne
\begin{equation}
    \frac{\varphi_0}{L} \left( \varphi_{L_1} + \varphi_{L_2} + \varphi_{L_3} + \varphi_{L_4} \right) = 0.
\end{equation}

\pagebreak
En introduisant les modes $\varphi_X$, $\varphi_Y$, $\varphi_Z$ et
$\varphi_M$ tels que définis [@eq:normal_modes], on a alors
\begin{equation}
    \varphi_M = \varphi_{\text{ext}} + 2 \pi \sum_{k = a, b, c, d} n_k.
\end{equation}

Contrairement au cas sans inductances de _shunt_ ([@eq:JRM_mailles]), les flux
$\varphi_{a, b, c, d}$ sont chacun $2\pi$-périodique ([@eq:JRM_shunt_mailles])
et le hamiltonien du système se simplifie alors en

$$\begin{split}
    H = & -4 E_J \left(
            \cos \frac{\varphi_X}{2} \cos \frac{\varphi_Y}{2} \cos \varphi_Z \cos \frac{\varphi_{\text{ext}}}{4} +
            \sin \frac{\varphi_X}{2} \sin \frac{\varphi_Y}{2} \sin \varphi_Z \sin \frac{\varphi_{\text{ext}}}{4}
        \right) \\
        & + \frac{E_L}{2} \left( \frac{\varphi_X^2}{2} + \frac{\varphi_Y}{2} + \varphi_Z^2\right). \\
\end{split}$${#eq:hamiltonian-JRM-shunt}

Il n'y a plus de dégénérescence des niveaux d'énergie, et toutes les
configurations des flux sont stables, permettant d'atteindre le point de
fonctionnement idéal.


# Modulateur en anneau Josephson asymétrique {#sec:asymmetricJRM}

L'objectif est alors de modifier légèrement ce circuit pour que son
hamiltonien soit celui nécessaire afin de stabiliser des états de chats,
c'est-à-dire de la forme $a^4 b^{\dagger} + \left(a^{\dagger}\right)^4 b$.
[@NJP2014] propose de dissymétriser le modulateur en anneau Josephson présenté
dans la section précédente ([@sec:JRM-shunt]), en utilisant un facteur 3 entre
les tailles des boucles ([@fig:asymmetric_shunted_JRM]).

![Schéma électrique du modulateur en anneau Josephson asymétrique, tel que
présenté dans @NJP2014. On note $E_L$ l'énergie associée aux
inductances de _shunt_ et $E_J$ celle associée aux jonctions
Josephson.](img/asymmetric_shunted_JRM.svg){#fig:asymmetric_shunted_JRM}

En suivant la méthode de la [@sec:JRM-shunt], les lois de Kirchhoff dans
chaque maille du circuit donnent :
\begin{equation}
    \left\{\begin{array}{c}
        \varphi_a - \varphi_{L_1} + \varphi_{L_4} = \frac{3 \varphi_{\text{ext}}}{8} + 2\pi n_a \\
        \varphi_b - \varphi_{L_4} + \varphi_{L_2} = \frac{\varphi_{\text{ext}}}{8} + 2\pi n_b \\
        \varphi_c - \varphi_{L_2} + \varphi_{L_3} = \frac{3 \varphi_{\text{ext}}}{8} + 2\pi n_c \\
        \varphi_d - \varphi_{L_3} + \varphi_{L_1} = \frac{\varphi_{\text{ext}}}{8} + 2\pi n_d
    \end{array}\right.
\end{equation}
où nous avons défini $\varphi_{\text{ext}}$ comme le flux extérieur traversant
la totalité de l'anneau.

Le hamiltonien s'écrit
$$\begin{split}
    H = & -E_J \left(
            \cos \varphi_a + \cos \varphi_b +
            \cos \varphi_c + \cos \varphi_d \right) \\
        & + \frac{E_L}{2} \left(
            \varphi_{L_1}^2 + \varphi_{L_2}^2 +
            \varphi_{L_3}^2 + \varphi_{L_4}^2 \right) \\
\end{split}
$${#eq:hamiltonian_asymmetric}

\pagebreak
En introduisant les modes $\varphi_X$, $\varphi_Y$, $\varphi_Z$ définis
[@eq:normal_modes], la partie inductive s'écrit toujours
\begin{equation}
    \varphi_{L_1}^2 + \varphi_{L_2}^2 + \varphi_{L_3}^2 + \varphi_{L_4}^2 =
    \frac{\varphi_X^2}{2} + \frac{\varphi_Y^2}{2} + \varphi_Z^2.
\end{equation}

Dans le cas où le flux extérieur $\varphi_{\text{ext}}$ est fixé à $2\pi$, la
contribution Josephson du hamiltonien se simplifie et a une expression très
différente du cas symétrique:
\begin{equation}\begin{split}
    \cos \varphi_a + \cos \varphi_b + \cos \varphi_c + \cos \varphi_d =
        & \cos \left( \frac{3\pi}{4} + \frac{\varphi_X + \varphi_Y + 2\varphi_Z}{2} \right) \\
%
        & + \cos \left( \frac{\pi}{4} - \frac{\varphi_X - \varphi_Y + 2\varphi_Z}{2} \right) \\
%
        & + \cos \left( \frac{3\pi}{4} - \frac{\varphi_X + \varphi_Y - 2\varphi_Z}{2} \right) \\
%
        & + \cos \left( \frac{\pi}{4} + \frac{\varphi_X - \varphi_Y - 2\varphi_Z}{2} \right) \\
%
        & = \dots \\
        %
        & = 2 \sqrt{2} \sin \left( \frac{\varphi_X}{2} \right) \sin \left( \frac{\varphi_Y}{2} \right) \Bigl[ \cos \varphi_Z - \sin \varphi_Z \Bigr]. \\
\end{split}\end{equation}


Finalement, le hamiltonien complet du système peut s'écrire
\begin{equation}\begin{split}
    H = & -2 \sqrt{2} \sin \left( \frac{\varphi_X}{2} \right) \sin \left( \frac{\varphi_Y}{2} \right) \Bigl[ \cos \varphi_Z - \sin \varphi_Z \Bigr] \\
    & + \frac{E_L}{4} \left[ \varphi_X^2 + \varphi_Y^2 + 2 \varphi_Z^2 \right]. \\
\end{split}\end{equation}

Notons que pour arriver à cette expression du hamiltonien, il est nécessaire
d'imposer $\varphi_{\text{ext}} = 2 \pi$. En effet, il n'est pas possible
d'obtenir une telle expression factorisée dans le cas général. C'est une
limitation importante par rapport au cas du modulateur en anneau Josephson
avec inductances de _shunt_, pour lequel le flux extérieur
$\varphi_{\text{ext}}$ était un paramètre de contrôle supplémentaire
([@eq:hamiltonian-JRM-shunt]), permettant de contrôler l'importance relative
des termes en cosinus et sinus, par les termes $\cos \left(
\frac{\varphi_{\text{ext}}}{4} \right)$ (resp. $\sin \left(
\frac{\varphi_{\text{ext}}}{4} \right)$).

On s'intéresse maintenant au hamiltonien du JRM asymétrique couplé à des modes
spatiaux par des antennes ([@fig:minimal_circuit_JRM]).

![Circuit minimal autour d'un modulateur en anneau Josephson asymétrique avec
inductances de _shunt_, similaire à celui présenté dans
[@ThèseManu].](img/asymmetric_shunted_JRM_circuit.svg){#fig:minimal_circuit_JRM}



Afin d'alléger les notations, introduisons les phases et charges aux nœuds
extérieurs de ce circuit, de façon analogue aux modes $X$, $Y$ et $Z$ définis
[@eq:normal_modes]
\begin{equation}\left\{\begin{array}{l}
    \Phi_X = \Phi_1 - \Phi_2 \\
    \Phi_Y = \Phi_3 - \Phi_4 \\
    \Phi_Z = \frac{\Phi_1 + \Phi_2 - \Phi_3 - \Phi_4}{2} \\
    Q_X = \frac{Q_1 - Q_2}{2} \\
    Q_Y = \frac{Q_3 - Q_4}{2} \\
    Q_Z = \frac{Q_1 + Q_2 - Q_3 - Q_4}{2} \\
\end{array}\right.\end{equation}

Introduisons également des inductances totales pour chaque mode, somme de la
contribution du _JRM_ et des antennes,
\begin{equation}\left\{\begin{array}{l}
    L_X^{\text{tot}} = L_a + L_X^{\text{JRM}} \\
    L_Y^{\text{tot}} = L_b + L_Y^{\text{JRM}} \\
    L_Z^{\text{tot}} = \frac{L_a + L_b}{4} + L_Z^{\text{JRM}} \\
\end{array}\right.\end{equation}
où $L_{X, Y, Z}^{\text{JRM}}$ est obtenu en prenant la partie inductive dans
le hamiltonien [@eq:hamiltonian_asymmetric] du _JRM_, après développement
limité à l'ordre deux :
$$ \left\{\begin{array}{l}
    L_X^{\text{JRM}} = L_Y^{\text{JRM}} = 2 \frac{\varphi_0^2}{E_L} \\
    L_Z^{\text{JRM}} = \frac{\varphi_0^2}{E_L} \\
\end{array}\right.$${#eq:inductances-JRM}

Le hamiltonien complet du circuit est alors donné par
\begin{equation}\begin{split}
    H = & -2 \sqrt{2} \sin \left( \frac{\varphi_X}{2} \right) \sin \left( \frac{\varphi_Y}{2} \right) \Bigl[ \cos \varphi_Z - \sin \varphi_Z \Bigr] \\
    & + \frac{E_L}{4} \left[ \varphi_X^2 + \varphi_Y^2 + 2 \varphi_Z^2 \right] \\
    & + \frac{\Phi_X^2}{2 L_X^{\text{tot}}} + \frac{Q_X^2}{2 C_a}
      + \frac{\Phi_Y^2}{2 L_Y^{\text{tot}}} + \frac{Q_Y^2}{2 C_b}
      + \frac{\Phi_Z^2}{2 L_Z^{\text{tot}}} + \frac{Q_Z^2}{2 C_c} \\
\end{split}\end{equation}
où
\begin{equation}
    C_c = \frac{4 C_a C_b}{C_a + C_b}.
\end{equation}

On a de plus la relation entre les nœuds extérieurs et les nœuds du _JRM_:
\begin{equation}
    \varphi_k = \frac{\Phi_k}{\varphi_0} \xi_k
\end{equation}
en définissant les facteurs de participation
\begin{equation}
    \xi_k = \frac{\varphi_0 \varphi_k}{\Phi_k} = \frac{L_k^{\text{JRM}}}{L_k^{\text{tot}}}.
\end{equation}

\pagebreak
On peut alors quantifier ce circuit en suivant la méthode décrite dans
[@GirvinCQED; @Houches_fluctuations]. On introduit les opérateurs de création
et annihilation de chaque mode afin de quantifier les phases:
\begin{equation}
    \left\{\begin{array}{c}
        \varphi_X = \varphi_X^0 \left( a + a^{\dagger} \right) \\
        \varphi_Y = \varphi_Y^0 \left( b + b^{\dagger} \right) \\
        \varphi_Z = \varphi_Z^0 \left( c + c^{\dagger} \right) \\
    \end{array}\right.
\end{equation}
avec
\begin{equation}
    \varphi_{X, Y, Z}^0 = \frac{\xi_{X, Y, Z}}{\varphi_0} \sqrt{\frac{\hbar \times Z_{X, Y, Z}}{2}} = \frac{\xi_{X, Y, Z}}{\varphi_0} \sqrt{\frac{\hbar}{2} \times L_{X, Y, Z}^{\text{tot}} \times \omega_{a, b, c}}
\end{equation}
où
\begin{equation}
    Z_{X, Y, Z} = \sqrt{\frac{L_{X, Y, Z}^{\text{tot}}}{C_{a, b, c}}} =
    L_{X, Y, Z}^{\text{tot}} \times \omega_{a, b, c}.
\end{equation}

À ce stade, notons également qu'a priori, $L_{X, Y, Z}^{\text{tot}}$ dépend du
flux extérieur $\varphi_{\text{ext}}$. La pulsation de résonance de chaque
mode dépend alors en théorie de ce même flux extérieur, par la relation
$\omega_{a, b, c} \left( \varphi_{\text{ext}} \right) = \frac{1}{\sqrt{L_{X,
Y, Z}^{\text{tot}} \left( \varphi_{\text{ext}} \right) \, C_{a, b, c}}}$.
Cependant, notre étude précédente nous a amené à devoir imposer
$\varphi_{\text{ext}} = 2 \pi$ et donc à supprimer ce paramètre de contrôle. Les
pulsations de résonance de chacun des modes sont donc imposées, et on n'a plus
aucun moyen de modifier le flux extérieur afin de corriger de faibles
désaccords de fréquence, comme il était possible de le faire sur un modulateur
en anneau Josephson symétrique avec inductances de _shunt_.

De plus, si $\varphi_X$, $\varphi_Y$ et $\varphi_Z$ sont petits, on peut
utiliser un développement limité de $\cos$ et $\sin$. Cette hypothèse est bien
vérifiée en prenant des paramètres de circuit du même ordre de grandeur que
ceux de [@ThèseFlavius; @ThèseManu]. En faisant un tel développement limité
jusqu'en $O\left(\|\boldsymbol{\varphi}\|^7\right)$, on a
\begin{equation} \begin{split}
    \sin \left( \frac{\varphi_X}{2} \right) \sin \left( \frac{\varphi_Y}{2} \right) \left[ \cos \varphi_Z - \sin \varphi_Z \right] = & %
            \frac{\varphi_X}{2} \frac{\varphi_Y}{2} - %
            \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z - %
            \frac{1}{3!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^3 \\
%
        & - \frac{1}{3!} \left( \frac{\varphi_X}{2} \right)^3 \frac{\varphi_Y}{2} - %
            \frac{1}{2} \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z^2 \\
%
        & + \frac{1}{3!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^3 \varphi_Z + %
            \frac{1}{3!} \left( \frac{\varphi_X}{2} \right)^3 \frac{\varphi_Y}{2} \varphi_Z \\
%
        & + \frac{1}{3!} \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z^3 + %
            \frac{1}{(3!)^2} \left( \frac{\varphi_X}{2} \right)^3 \left( \frac{\varphi_Y}{2} \right)^3 \\
%
        & + \frac{1}{5!} \left( \frac{\varphi_X}{2} \right)^5 \frac{\varphi_Y}{2} + %
            \frac{1}{5!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^5 \\
%
        & + \frac{1}{2 \times 3!} \frac{\varphi_X}{2} \left( \frac{\varphi_Y}{2} \right)^3 \varphi_Z^2 + %
            \frac{1}{2 \times 3!} \left( \frac{\varphi_X}{2} \right)^3 \frac{\varphi_Y}{2} \varphi_Z^2 \\
%
        & + \frac{1}{4!} \frac{\varphi_X}{2} \frac{\varphi_Y}{2} \varphi_Z^4
\end{split}\end{equation}

En utilisant l'approximation de champ tournant [^quantsys-footnote] au premier
ordre, on ne garde que les termes non oscillants, c'est à dire les termes
constants et les termes oscillants à $\omega_a - 4 \omega_c + \omega_b = 0$
(d'après la condition d'accord des fréquences).

[^quantsys-footnote]: Se reporter notamment à [@QuantSys2015, pp.15-20] pour
  une présentation de la méthode et un rappel des formules utiles.

_Note_: On remarque qu'en tronquant le développement à l'ordre 4, et en
modifiant la condition d'accord de fréquence en $\omega_a - 2 \omega_c +
\omega_b = 0$, on ne garde que le terme en $\frac{1}{2} \frac{\varphi_X}{2}
\frac{\varphi_Y}{2} \varphi_Z^2$, qui donnera un hamiltonien d'échange à deux
photons de la forme $\hbar\,g_{\text{2ph}} \left( b \left(c^{\dagger}\right)^2
+ \text{h.c.}\right)$, utilisable également pour le procotole à deux photons
décrit dans [@NJP2014]. L'étude théorique de ce circuit utilisé dans ce cadre,
et notamment la comparaison des performances avec celles d'une seule jonction
Josephson entre deux cavités comme utilisée dans [@ZakiScience2015], n'a pas pu
être réalisée durant le stage, faute de temps, et fera l'objet d'un travail
ultérieur.

En remplaçant les opérateurs $\varphi_{X, Y, Z}$ par leur expression en termes
d'opérateurs création, et dans l'approximation de champ tournant au premier
ordre, l'évolution dans le référentiel tournant associé à $H_0 = \hbar
\omega_a a^{\dagger} a + \hbar \omega_b b^{\dagger}b + \hbar \omega_c
c^{\dagger}c$ est donnée par
$$
    H_{RWA} = - \frac{\sqrt{2}}{48} E_J \times \varphi_X^0 \, \varphi_Y^0
              \left( \varphi_Z^0 \right)^4 \times \left[
                    a b \left( c^{\dagger} \right)^4 + \text{h.c.}
              \right]
$${#eq:hamiltonian_RWA}

Le mode $a$ dans l'[@eq:hamiltonian_RWA] sert alors de pompe, le mode $b$ sert
de mode de lecture (réservoir) et le mode $c$ sert de mode de stockage.

Notons enfin que le système est stable tant que les dérivées
partielles du hamiltonien par rapport aux modes propres $\varphi_{X, Y, Z}$,
$\frac{\partial^2 H}{\partial \varphi_{X, Y, Z}^2}$, sont positives, c'est à
dire tant que le système est dans un minimum de potentiel. Cela revient à
vouloir que les inductances associées à chaque mode $L^{\text{JRM}}_{X, Y, Z}$
[@eq:inductances-JRM] soient toutes positives. D'après leur expression, c'est
toujours le cas et le circuit est donc toujours stable, contrairement au cas
du JRM symétrique pour lequel les inductances $L^{\text{JRM}}_{X, Y, Z}$
dépendent du flux extérieur $\varphi_{\text{ext}}$ [@ThèseManu, sec. 2.3.3].


_Note_: Dans le cas d'un rapport quelconque $\lambda$ au lieu de 3 entre les
surfaces des petites et des grandes boucles, la partie Josephson du
hamiltonien devient, toujours en imposant $\varphi_{\text{ext}} = 2 \pi$,
\begin{equation}
    -4E_J \sin \left( \frac{\varphi_X}{2} \right) \sin \left( \frac{\varphi_Y}{2} \right) \Bigl[ \sin \left( \frac{\lambda - 1}{2\left( \lambda + 1\right)} \pi \right) \cos \varphi_Z - \cos \left( \frac{\lambda - 1}{2\left( \lambda + 1\right)} \pi \right) \sin \varphi_Z \Bigr]
\end{equation}
En jouant sur le rapport des surfaces, on peut donc modifier la prépondérance
du terme $\cos \varphi_Z$ ou $\sin \varphi_Z$, et ainsi potentiellement
réduire le poids de termes parasites. \newline
On remarque aussi que la symétrie forcée par l'alternance des petites et des
grandes boucles impose le mode qui sera présent dans le cosinus. En utilisant
la configuration de la [@fig:asymmetric_shunted_JRM], c'est le mode commun $Z$
qui apparaît dans le terme en cosinus et servirait donc de mode de stockage.
Au contraire, en mettant les deux grandes boucles côte à côte
([@fig:JRM_bottom]), ce sera le mode différentiel $X$ qui apparaîtra dans le
cosinus. Le hamiltonien du circuit présenté [@fig:JRM_bottom] est en effet:
\begin{equation}\begin{split}
    H = & -2 \sqrt{2} \sin \left( \frac{\varphi_Y}{2} \right) \sin \left( \varphi_Z \right) \Bigl[ \cos \left( \frac{\varphi_X}{2} \right) + \sin \left( \frac{\varphi_X}{2} \right) \Bigr] \\
    & + \frac{E_L}{4} \left[ \varphi_X^2 + \varphi_Y^2 + 2 \varphi_Z^2 \right]. \\
\end{split}\end{equation}

![Le mode qui apparaît dans le terme en cosinus peut être modifié en changeant
l'alternance des boucles. Ainsi, cette configuration permet de faire
apparaître le mode $X$ dans le terme en
cosinus.](img/asymmetric_shunted_JRM_bottom.svg){#fig:JRM_bottom}


\pagebreak

# Amplitude du terme de couplage

On considère que le mode $a$ est couplé à un mode de pompe de façon non
résonante et avec une amplitude bien plus importante que celle des autres
modes, $b$ et $c$. Ainsi, nous souhaitons que la dynamique des ces autres
modes n'affecte pas la dynamique du mode $a$, et on peut le traiter de façon
quasi-classique. Ceci constitue l'approximation de pompe rigide. On peut alors
réécrire le hamiltonien après quantification et dans l'approximation de champ
tournant du _JRM_ asymétrique ([@eq:hamiltonian_RWA]) et on obtient
\begin{equation}
    H = H_0 +
        \hbar \, g_{\text{4ph}} \left[ b \left(c^{\dagger}\right)^4 + b^{\dagger} c^4 \right]
\end{equation}
où
\begin{equation}
    H_0 = \hbar \omega_b b^{\dagger}b + \hbar \omega_c c^{\dagger}c
\end{equation}
et
$$
    g_{\text{4ph}} = \frac{\sqrt{2}}{48} \qquad \times \underbrace{\frac{E_J}{E_L} \sqrt{\xi_Y \xi_Z} \sqrt{\omega_b \omega_c}}_{\substack{\text{préfacteur du mélange à trois ondes}\\\text{d'un \textit{JRM} symétrique}}} \times \qquad
                 \left( \frac{\xi_Z \omega_c}{E_L / \hbar} \right)^{3/2}.
$${#eq:chi}
Dans cette expression, l'amplitude du mode de pompe $a$ n'intervient pas car on
suppose qu'on peut le pousser suffisamment pour pouvoir considérer que $\sin
\left( \frac{\varphi_X}{2} \right) \approx 1$, grâce à la condition de pompe
rigide et l'analyse de stabilité de la fin de la [@sec:asymmetricJRM].

Le but maintenant est d'évaluer l'ordre de grandeur du terme de couplage, ce
qui revient à évaluer l'ordre de grandeur de $g_{\text{4ph}}$. Un premier
calcul rapide, en utilisant les valeurs de [@ThèseManu; @ThèseFlavius] ($E_L
\approx 10^{15} \text{J}$ qui correspond à des inductances de _shunt_ de
$30\text{pH}$, $E_J \approx E_L$, ${\omega_b \approx \omega_c \approx 10^{10}
\text{Hz}}$ et $\xi_Y \approx \xi_Z \approx 0.1$), donne $g_{\text{4ph}}
\approx 1\text{Hz}$, ce qui est bien trop faible. Cette valeur très faible est
principalement due au dernier terme de l'[@eq:chi], qui est de l'ordre de
$10^{-9}$.

Une partie de mon stage a donc été consacrée à la recherche de solutions afin
d'augmenter la valeur de cette constante de couplage, pour atteindre des
valeurs de l'ordre du MHz, idéalement.

## Optimisation des paramètres du circuit {#sec:optimisation-valeurs}

Tout d'abord, au vu de l'expression de $g_{\text{4ph}}$ ([@eq:chi]) et en
prenant en compte les différentes symétries, on peut agir sur les constantes
suivantes :

* $E_J$, par la taille des jonctions Josephson, qu'on préfèrerait garder
fixée ici pour des raisons pratiques de nanofabrication.
* $E_L$, par la valeur des inductances de _shunt_.
* $L_a$ et $L_b$, les inductances dans les antennes.


Une première amélioration possible est d'augmenter la valeur des facteurs de
participation, pour passer d'une valeur proche de $0.1$ à $1/2$. De tels
facteurs de participation peuvent être atteints en prenant

\begin{equation}
    L_a = L_X^{\text{JRM}} = 2 \frac{\varphi_0^2}{E_L} \text{\qquad et \qquad} L_b = L_Y^{\text{JRM}} = 2 \frac{\varphi_0^2}{E_L}.
\end{equation}

Comme $g_{\text{4ph}} \propto \xi_Y^{1/2} \xi_Z^{5/2}$, multiplier par un facteur $5$
les facteurs de participation fait gagner un facteur $100$ sur $g_{\text{4ph}}$. Ce
n'est cependant toujours pas suffisant pour atteindre le régime souhaité.

On peut alors envisager de modifier $E_L$. En remplaçant tous les termes
intervenant dans l'expression de $g_{\text{4ph}}$ par les valeurs numériques de
[@ThèseManu, @ThèseFlavius], et dans la limite $\xi_{Y, Z} = 1/2$ précédente,
on obtient

\begin{equation}
    g_{\text{4ph}} \approx \left(\frac{L}{1 \, \text{H}}\right)^{5/2} \times \left(2.4 \times 10^{28} \, \text{Hz}\right),
\end{equation}

où $L = \frac{\varphi_0^2}{E_L}$ est l'inductance de _shunt_.


Pour pouvoir atteindre $g_{\text{4ph}} \approx 1\text{MHz}$, il faut donc avoir $L
\approx 1 \text{nH}$, soit des inductances trente fois plus grandes que celles
utilisées habituellement [@ThèseManu; @ThèseFlavius]. Une telle valeur
d'inductance peut être réalisée à l'aide d'une chaîne de jonctions Josephson
mais commence à poser des problèmes pratiques dus à la complexité de la
nanofabrication et l'encombrement de ces chaînes de jonctions, qui sont
censées se situer à l'intérieur des branches contenant les jonctions Josephson
principales du modulateur en anneau Josephson.

D'autre part, pour une telle valeur des inductances de _shunt_, on obtient
\begin{equation}\left\{\begin{array}{c}
    |\varphi_Y| \approx 0.13 \text{ (contre $0.01$ initialement)}\\
    |\varphi_Z| \approx 0.05 \text{ (contre $0.003$ initialement)}\\
\end{array}\right., \end{equation}
ce qui représente une augmentation d'un facteur $10$ de l'amplitude des
phases $\varphi_Y^0$ et $\varphi_Z^0$. Le développement limité réalisé dans la
[@sec:asymmetricJRM] n'est donc potentiellement plus valable, et il peut
falloir prendre en compte plus de termes dans le développement du hamiltonien,
ce qui est étudié dans la section suivante.


## Développement complet dans la limite de l'approximation de champ tournant {#sec:completeExpansion}

Dans le calcul de la [@sec:asymmetricJRM], on n'a gardé que
les termes les plus bas dans les développements de $\cos$ et $\sin$, en
tronquant au premier terme nécessaire afin de faire apparaître le terme
d'échange à quatre photons, puis on a appliqué l'approximation de champ
tournant à ce développement. Cependant, les calculs de la
[@sec:optimisation-valeurs] amènent à avoir des valeurs beaucoup plus
importantes pour les fluctuations de point zéro de chaque mode, et le
développement aux ordres les plus bas n'est potentiellement plus valable.

Sous certaines conditions d'incommensurabilité, considérer les termes d'ordre
supérieur dans le développement revient à prendre en compte tous les termes de
la forme
\begin{equation}
    \left(a^{\dagger}\right)^k a^k
    \left(b^{\dagger}\right)^l b^l
    \left(c^{\dagger}\right)^m c^m
    \left[ a b \left(c^{\dagger}\right)^4 + a^{\dagger} b^{\dagger} c^4 \right]
\end{equation}
où $k, l, m \in \mathbb{Z}$, dans le développement de
\begin{equation}
    \sin \left( \varphi_a^0 \times \frac{a + a^{\dagger}}{2} \right) \times
    \sin \left( \varphi_b^0 \times \frac{b + b^{\dagger}}{2} \right) \times
    \cos \left( \varphi_c^0 \times \frac{c + c^{\dagger}}{2} \right).
\end{equation}

Ces termes vérifient automatiquement la condition d'accord des fréquences.

En conservant ces termes dans le développement, et en traitant uniquement le
mode de pompe semi-classiquement, on obtient
\begin{equation}\begin{split}
    & \left[ %
        \frac{\varphi_a^0}{2} \times e^{-\left(\varphi_a^0\right)^2 / 8} \times %
        \frac{J_1\left(\varphi_a^0 |\alpha|\right)}{\left(\varphi_a^0 |\alpha|\right) / 2} %
    \right] \times \\
    & \left[ %
        - \frac{\varphi_b^0}{2} \times e^{-\left(\varphi_b^0\right)^2 / 8} \times %
        \frac{L_{a^{\dagger} a + 1}^{(-1)}\left(\left(\varphi_b^0\right)^2 / 4\right)}{\left(\varphi_b^0\right)^2 / 4} %
    \right] \times \\
    & \left[ %
        e^{-\left(\varphi_c^0\right)^2 / 2 } \times %
        \frac{L_{c^{\dagger} c}^{(-4)}\left({\varphi_c^0}^2\right)}{\left(\varphi_c^0\right)^4} %
    \right] \times \\
    & \left[\alpha \times b \left(c^{\dagger}\right)^4 + \text{h.c.}\right] \\
\end{split}\end{equation}

\pagebreak

Si on s'intéresse uniquement au mode de pompe, on veut maximiser le terme
([@fig:gpompe])
\begin{equation}
    g_{\text{pompe}} \left( \alpha \right) = \left[
        \frac{\varphi_a^0}{2} \times e^{-\left(\varphi_a^0\right)^2 / 8} \times
        \frac{J_1\left(\varphi_a^0 |\alpha|\right)}{\left(\varphi_a^0 |\alpha|\right) / 2}
    \right] \times \left(\alpha + \alpha^*\right),
\end{equation}
ce qui revient à maximiser $J_1\left( \varphi_a^0 |\alpha|\right)$, par rapport à $|\alpha|$.

On trouve alors, en prenant $\alpha$ réel et en optimisant ([@fig:optimal_mode_a])
\begin{equation}
    |\alpha_{\text{optimal}}|^2 = \left[ \frac{1}{\varphi_a^0} \times \frac{\mathrm{d} J_1 \left( \varphi_a^0 |\alpha| \right)}{\mathrm{d} |\alpha|} \right]^2 = \left[\frac{\text{Root}\left[J_1(x) = x \times J_0(x)\right]}{\varphi_a^0}\right]^2 \approx \left[\frac{1.851\dots}{\varphi_a^0}\right]^2.
\end{equation}

\begin{figure}[h!]
    \centering
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics{img/gpompe.pdf}
        \caption{}
        \label{fig:gpompe}
    \end{subfigure}
    ~
    \begin{subfigure}[t]{0.49\textwidth}
        \centering
        \includegraphics{img/optimal_pump_number_photons.pdf}
        \caption{}
        \label{fig:optimal_mode_a}
    \end{subfigure}
    \caption{(\ref{fig:gpompe}) Contribution du mode de pompe $a$, en fonction du nombre de photons dans ce mode, $|\alpha|^2$. On considère uniquement des états cohérents $\left|\alpha\right>$ avec une amplitude réelle. On a pris $\varphi_a^0 = 0.1$ pour tracer cette courbe. (\ref{fig:optimal_mode_a}) Nombre optimal de photons dans le mode de pompe $a$, $\left\langle a^{\dagger} a \right\rangle$, en fonction de $\varphi_a^0$. }
\end{figure}


On peut donc atteindre des valeurs de l'ordre de l'unité pour ce préfacteur,
en poussant suffisamment l'amplitude de la pompe.

Dans la suite, nous allons traiter le préfacteur devant le terme $a b
\left(c^{\dagger}\right)^4 + \text{h.c.}$ de manière semi-classique, en
remplaçant les opérateurs $a$, $b$ et $c$ par des nombres complexes $\alpha$,
$\beta$ et $\gamma$. On obtient alors
$$\begin{split}
    &\left[
        \frac{\varphi_a^0}{2} \times e^{-\left(\varphi_a^0\right)^2 / 8} \times
        \frac{J_1\left(\varphi_a^0 |\alpha|\right)}{\left(\varphi_a^0 |\alpha|\right) / 2}
    \right] \times \\
    & \left[
        \frac{\varphi_b^0}{2} \times e^{-\left(\varphi_b^0\right)^2 / 8} \times
        \frac{J_1\left(\varphi_b^0 |\beta|\right)}{\left(\varphi_b^0 |\beta|\right) / 2}
    \right] \times \\
    & \underbrace{\left[
        \left(\varphi_c^0\right)^4 \times e^{-\left(\varphi_c^0\right)^2 / 2} \times
        \frac{J_4\left(2\varphi_c^0 |\gamma|\right)}{\left(\varphi_c^0 |\gamma|\right)^4}
    \right]}_{=\, G \left( \varphi, \gamma \right) \times \frac{\left(\varphi_c^0\right)^4}{4!}} \times \\
    & \left[ a b \left(c^{\dagger}\right)^4 + \text{h.c.}\right] \\
    \end{split}$${#eq:full_expansion}
où $\left| \alpha \right>$, $\left| \beta \right>$ et $\left| \gamma \right>$
sont les états cohérents dans les modes $X$, $Y$ et $Z$ respectivement.

_Note_: Cette étude ne nous permet pas de faire des simulations de l'évolution
purement quantique du système, et en particulier d'étudier l'influence de ces
termes supplémentaires, tels que le terme en $c^{\dagger} c \left( c^4 +
\text{h.c.} \right)$ sur les états de chat. N'ayant pas eu le temps de
réaliser cette étude durant ce stage, elle sera faite dans un travail
ultérieur.

On peut alors s'intéresser aux autres modes et essayer d'augmenter les
préfacteurs associés (et donc la valeur de $g_{\text{4ph}}$) en jouant de même
sur les amplitudes des états cohérents. Si on s'intéresse au développement sur
le mode $c$, on peut tracer le coefficient devant les opérateurs en fonction
de l'état cohérent $\left|\gamma\right\rangle$ et de la valeur de
$\varphi_c^0$ ([@fig:expansion_c]).

![Gain dans l'[@eq:full_expansion] dû au développement du mode $c$, en fonction
de $\gamma$, l'amplitude de l'état cohérent $\left| \gamma \right\rangle$ dans
le mode $c$ et de $\varphi_c^0$ : $G(\varphi, \gamma) = 24 \times
e^{-\varphi^2 / 2} \times \frac{J_4\left(2 \varphi |\gamma|\right)}{(\varphi
|\gamma|)^4}$.](img/gain_expansion_c.pdf){height=43% #fig:expansion_c}

![Amplitude de la contribution du mode $c$ dans [@eq:full_expansion] en fonction de
$\gamma$, l'amplitude de l'état cohérent $\left| \gamma \right\rangle$ dans le
mode $c$ et de $\varphi_c^0$ : $G(\varphi, \gamma) \times
\frac{\left(\varphi_c^0\right)^4}{4!} = \left(\varphi_c^0\right)^4 \times
e^{-\varphi^2 / 2} \times \frac{J_4\left(2 \varphi |\gamma|\right)}{(\varphi
|\gamma|)^4}$.](img/gain_expansion_c_full.pdf){height=43% #fig:expansion_c}

En prenant en compte le développement complet de cette section avec un état
cohérent $|\alpha| = 2$, on aurait ${g_{\text{4ph}} \approx 4.6 \times 10^5
\text{Hz}}$ avec les valeurs « optimales » de la [@sec:optimisation-valeurs].
Ce résultat diffère du résultat de la [@sec:optimisation-valeurs] seulement
d'un facteur 2 et reste d'un ordre de grandeur acceptable au vu de nos
contraintes.

\pagebreak

# Conclusion

Ce stage a été l'occasion de découvrir le domaine des circuits
supraconducteurs, n'ayant jusqu'à présent eu l'occasion de travailler qu'avec
des systèmes d'optique atomique. J'ai eu l'occasion de découvrir de nouveaux
outils et méthodes, tels que les méthodes de quantification d'un circuit et
d'ingénierie de réservoir.

Après m'être familiarisé avec le formalisme et les méthodes, en reprenant
l'étude du modulateur à anneaux Josephson avec des inductances de _shunt_,
j'ai eu l'occasion de travailler sur une version asymétrisée de ce circuit,
pour en étudier les propriétés et les améliorations possibles pour l'utiliser
afin de réaliser expérimentalement un hamiltonien d'échange à quatre photons.

Nous avons ainsi vu que ce circuit n'était pas directement utilisable dans ce
but car on obtiendrait alors une constante de couplage non-linéaire trop
faible, et avons cherché à y remédier. Un certain nombre de points n'ont
cependant pas été étudié, faute de temps, et feront l'objet d'un travail
ultérieur, dans le cadre d'une thèse débutant en septembre.

En particulier, un certain nombre d'approximations ont été faites afin de
réaliser cette étude, toujours à l'ordre le plus bas. Une étude systématique
de la validité de ces hypothèses, en particulier de l'approximation de champ
tournant au premier ordre, était prévue mais n'a pu être faite faute de temps.
Une des prochaines étapes est donc de déterminer si cette approximation est
suffisante ou s'il faut utiliser l'approximation du second ordre.

De plus, avant d'envisager une réalisation expérimentale, il faut modéliser et
simuler la convergence vers un état de chat de Schrödinger ainsi que la
stabilité d'un tel état. Il faut également réfléchir à la meilleure façon de
coupler des modes propageant avec les différents modes de ce circuit, de la
façon la plus efficace possible afin de garder les bonnes propriétés du
circuit.

Enfin, ce circuit peut également servir à réaliser un hamiltonien d'échange à
deux photons et une étude comparée de ce circuit et d'un montage avec une
unique jonction entre deux cavités est prévue.
